from django.urls import path
from apps.doctors.views import CreateAppointmentView


app_name = 'doctors'

urlpatterns = [
    path('', view=CreateAppointmentView.as_view(), name='create-appointment'),
]
