from typing import Dict

from apps.accounts.models import Client
from apps.doctors.models import Appointment


class AppointmentService:
    @staticmethod
    def create(vd: Dict) -> Appointment:
        client_data = {
            'first_name': vd.pop('first_name'),
            'last_name': vd.pop('last_name'),
            'middle_name': vd.pop('middle_name', None)
        }
        client = Client.objects.create(**client_data)
        return Appointment.objects.create(client=client, **vd)
