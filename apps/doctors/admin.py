from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from apps.doctors.models import Doctor, WorkSchedule, Appointment


class WorkScheduleAdmin(admin.TabularInline):
    model = WorkSchedule

    def get_queryset(self, request):
        return super().get_queryset(request).order_by('created_at')


@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {'fields': ('first_name', 'last_name', 'middle_name', 'is_active',)}),
        (_('important dates'), {'fields': ('created_at', 'updated_at',)}),
    )
    readonly_fields = ('created_at', 'updated_at',)
    list_display = ('id', 'first_name', 'last_name', 'middle_name', 'is_active', 'created_at', 'updated_at',)
    list_display_links = ('id',)
    ordering = ('-created_at',)
    inlines = (WorkScheduleAdmin,)


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', 'updated_at',)
    list_display = ('id', 'date', 'doctor', 'client', 'is_active', 'created_at', 'updated_at',)
    list_display_links = ('id',)
    ordering = ('-created_at',)

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False
