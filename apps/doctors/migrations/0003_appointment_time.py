# Generated by Django 3.1.3 on 2020-12-10 21:05

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('doctors', '0002_auto_20201210_2103'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='time',
            field=models.TimeField(default=django.utils.timezone.now, verbose_name='time'),
            preserve_default=False,
        ),
    ]
