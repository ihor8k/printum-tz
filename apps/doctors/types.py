import datetime

from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _


HOUR_CHOICES = [(datetime.time(hour=x), '{:02d}:00'.format(x)) for x in range(0, 24)]


class Weekdays(TextChoices):
    MONDAY = 'MONDAY', _('Monday')
    TUESDAY = 'TUESDAY', _('Tuesday')
    WEDNESDAY = 'WEDNESDAY', _('Wednesday')
    THURSDAY = 'THURSDAY', _('Thursday')
    FRIDAY = 'FRIDAY', _('Friday')
    SATURDAY = 'SATURDAY', _('Saturday')
    SUNDAY = 'SUNDAY', _('Sunday')


class ShiftType(TextChoices):
    FIRST = 'FIRST', _('First')
    LAST = 'LAST', _('Last')
