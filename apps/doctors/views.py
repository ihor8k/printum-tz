from django.views.generic.edit import FormView
from apps.doctors.forms import CreateAppointmentForm
from apps.doctors.services import AppointmentService


class CreateAppointmentView(FormView):
    template_name = 'create_appointment.html'
    form_class = CreateAppointmentForm
    success_url = '/'

    def form_valid(self, form):
        AppointmentService.create(form.cleaned_data)
        return super().form_valid(form)
