import pytest

from django.db.models import QuerySet
from django.utils.timezone import now
from apps.doctors.models import Appointment
from apps.doctors.services import AppointmentService


@pytest.mark.django_db
def tests_appointment_create(doctors: QuerySet) -> None:
    data = {
        'first_name': 'Test',
        'last_name': 'User',
        'middle_name': 'Client',
        'doctor': doctors.order_by('?').first(),
        'date': now().date(),
        'hour': '11:00:00'
    }
    AppointmentService.create(data)

    assert Appointment.objects.count() == 1
    assert data['doctor'].appointments.count() == 1
