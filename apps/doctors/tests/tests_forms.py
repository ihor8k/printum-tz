import pytest

from django.db.models import QuerySet
from django.utils.timezone import now

from apps.doctors.forms import CreateAppointmentForm
from apps.doctors.models import Doctor


@pytest.mark.django_db
def tests_create_appointment_form(work_schedule: QuerySet) -> None:
    data = {
        'first_name': 'Test',
        'last_name': 'User',
        'middle_name': 'Client',
        'doctor': Doctor.objects.order_by('?').first(),
        'date': now().date(),
        'hour': '11:00:00'
    }
    form = CreateAppointmentForm(data)
    assert form.is_valid()

    data['hour'] = '14:00:00'
    form = CreateAppointmentForm(data)
    assert form.is_valid()

    data['hour'] = '17:00:00'
    form = CreateAppointmentForm(data)
    assert form.is_valid()


@pytest.mark.django_db
def tests_create_appointment_form_error_case(work_schedule: QuerySet) -> None:
    data = {
        'first_name': 'Test',
        'last_name': 'User',
        'middle_name': 'Client',
        'doctor': Doctor.objects.order_by('?').first(),
        'date': now().date(),
        'hour': '06:00:00'
    }
    form = CreateAppointmentForm(data)
    assert not form.is_valid()

    data['hour'] = '13:00:00'
    form = CreateAppointmentForm(data)
    assert not form.is_valid()

    data['hour'] = '18:00:00'
    form = CreateAppointmentForm(data)
    assert not form.is_valid()

    data['hour'] = '08:00:00'
    form = CreateAppointmentForm(data)
    assert not form.is_valid()

    data['hour'] = '20:00:00'
    form = CreateAppointmentForm(data)
    assert not form.is_valid()
