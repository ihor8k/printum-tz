from django.db import models
from django.utils.translation import gettext_lazy as _
from apps.doctors.types import HOUR_CHOICES, Weekdays, ShiftType
from py_helpers.models import UUIDCreateUpdateActiveModel


class Doctor(UUIDCreateUpdateActiveModel):
    first_name = models.CharField(_('first name'), max_length=128)
    last_name = models.CharField(_('last name'), max_length=128)
    middle_name = models.CharField(_('middle name'), max_length=128, blank=True, null=True)

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return self.get_full_name()


class WorkSchedule(UUIDCreateUpdateActiveModel):
    doctor = models.ForeignKey('doctors.Doctor', verbose_name=_('doctor'), on_delete=models.CASCADE, related_name='work_schedule')
    day = models.CharField(_('day'), max_length=64, choices=Weekdays.choices)
    shift = models.CharField(_('shift'), max_length=64, choices=ShiftType.choices)
    start = models.TimeField(_('start'), choices=HOUR_CHOICES)
    end = models.TimeField(_('end'), choices=HOUR_CHOICES)

    class Meta:
        constraints = (models.UniqueConstraint(fields=['doctor', 'day', 'shift'], name='doctors_doctor_doctor_day_shift'),)


class Appointment(UUIDCreateUpdateActiveModel):
    doctor = models.ForeignKey('doctors.Doctor', verbose_name=_('doctor'), on_delete=models.CASCADE, related_name='appointments')
    date = models.DateField(_('date'))
    hour = models.TimeField(_('hour'), choices=HOUR_CHOICES)
    client = models.ForeignKey('accounts.Client', verbose_name=_('client'), on_delete=models.CASCADE, related_name='appointments')
