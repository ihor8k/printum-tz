from django import forms
from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from apps.doctors.models import Doctor, WorkSchedule, Appointment
from apps.doctors.types import HOUR_CHOICES


class CreateAppointmentForm(forms.Form):
    doctor = forms.ModelChoiceField(label=_('doctor'), queryset=Doctor.active.all())
    date = forms.DateField(label=_('date'))
    hour = forms.ChoiceField(label=_('hour'), choices=HOUR_CHOICES)
    first_name = forms.CharField(label=_('first name'), max_length=128)
    last_name = forms.CharField(label=_('last name'), max_length=128)
    middle_name = forms.CharField(label=_('middle name'), max_length=128, required=False)

    class Meta:
        fields = ('doctor', 'date', 'hour', 'first_name', 'last_name', 'middle_name',)

    def clean(self):
        cld = super().clean()
        day = cld['date'].strftime("%A")
        doctor_at_work = cld['doctor'].work_schedule.filter(
            (Q(start__lte=cld['hour']) & Q(end__gte=cld['hour']) & Q(is_active=True) & Q(day__iexact=day)) & ~Q(end=cld['hour'])).exists()
        if not doctor_at_work:
            self.add_error('doctor', _('the doctor does not work'))
            return cld
        if Appointment.objects.filter(Q(date=cld['date']) & Q(hour=cld['hour'])).exists():
            self.add_error('hour', _('this hour is busy'))
            return cld
        return cld
