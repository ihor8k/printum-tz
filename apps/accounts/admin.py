from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from apps.accounts.models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {'fields': ('first_name', 'last_name', 'middle_name', 'is_active',)}),
        (_('important dates'), {'fields': ('created_at', 'updated_at',)}),
    )
    readonly_fields = ('created_at', 'updated_at',)
    list_display = ('id', 'first_name', 'last_name', 'middle_name', 'is_active', 'created_at', 'updated_at',)
    list_display_links = ('id',)
    ordering = ('-created_at',)
