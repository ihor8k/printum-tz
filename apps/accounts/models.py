from django.db import models
from django.utils.translation import gettext_lazy as _
from py_helpers.models import UUIDCreateUpdateActiveModel


class Client(UUIDCreateUpdateActiveModel):
    first_name = models.CharField(_('first name'), max_length=128)
    last_name = models.CharField(_('last name'), max_length=128)
    middle_name = models.CharField(_('middle name'), max_length=128, blank=True, null=True)

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def __str__(self):
        return self.get_full_name()
