#!/bin/sh

./manage.py dumpdata accounts.User --indent 4 > fixtures/accounts_User.json
./manage.py dumpdata authtoken.Token --indent 4 > fixtures/authtoken_Token.json
