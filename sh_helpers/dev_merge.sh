#!/usr/bin/env bash

CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
git checkout develop
git fetch
git pull
git branch -D ${CURRENT_BRANCH}