import os

from pathlib import Path


ROOT_DIR = Path(__file__).parent.parent.parent
APPS_DIR = ROOT_DIR / 'apps'

# General
DEPLOYMENT_ENVIRONMENT = os.getenv('DEPLOYMENT_ENVIRONMENT', 'PROD')
IS_PROD = True if DEPLOYMENT_ENVIRONMENT == 'PROD' else False
IS_DEV = True if DEPLOYMENT_ENVIRONMENT == 'DEV' else False
IS_TEST = True if DEPLOYMENT_ENVIRONMENT == 'TEST' else False

# https://docs.djangoproject.com/en/3.1/ref/settings/#debug
DEBUG = False if IS_PROD else True
# https://docs.djangoproject.com/en/ref/settings/#secret-key
SECRET_KEY = os.getenv('DJANGO_SECRET_KEY')
# https://docs.djangoproject.com/en/3.1/ref/settings/#allowed-hosts
ALLOWED_HOSTS = [v.strip() for v in os.getenv('DJANGO_ALLOWED_HOSTS').strip().split(',') if v]
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = 'Europe/Kiev'
# https://docs.djangoproject.com/en/3.1/ref/settings/#language-code
LANGUAGE_CODE = 'uk'
# https://docs.djangoproject.com/en/3.1/ref/settings/#use-i18n
USE_I18N = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#use-tz
USE_TZ = False
# https://docs.djangoproject.com/en/3.1/ref/settings/#append-slash
APPEND_SLASH = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#root-urlconf
ROOT_URLCONF = 'config.urls'
# https://docs.djangoproject.com/en/3.1/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# APPS
# https://docs.djangoproject.com/en/3.1/ref/settings/#installed-apps
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'django.contrib.messages',

    'apps.accounts',
    'apps.doctors',
]

# Auth
# https://docs.djangoproject.com/en/3.1/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend',)

# Password
# https://docs.djangoproject.com/en/3.1/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
]
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]

# Middleware
# https://docs.djangoproject.com/en/3.1/ref/settings/#middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# Security
# https://docs.djangoproject.com/en/3.1/ref/settings/#session-cookie-httponly
SESSION_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#csrf-cookie-httponly
CSRF_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True
# https://docs.djangoproject.com/en/3.1/ref/settings/#x-frame-options
X_FRAME_OPTIONS = 'DENY'
# https://docs.djangoproject.com/en/3.1/ref/settings/#secure-ssl-redirect
SECURE_SSL_REDIRECT = bool(os.getenv('DJANGO_SECURE_SSL_REDIRECT', False))
# https://docs.djangoproject.com/en/3.1/ref/settings/#session-cookie-secure
SESSION_COOKIE_SECURE = bool(os.getenv('DJANGO_SESSION_COOKIE_SECURE', False))
# https://docs.djangoproject.com/en/3.1/ref/settings/#csrf-cookie-secure
CSRF_COOKIE_SECURE = bool(os.getenv('DJANGO_CSRF_COOKIE_SECURE', False))
# https://docs.djangoproject.com/en/3.1/ref/settings/#secure-hsts-include-subdomains
SECURE_HSTS_INCLUDE_SUBDOMAINS = bool(os.getenv('DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS', False))
# https://docs.djangoproject.com/en/3.1/ref/settings/#secure-hsts-preload
SECURE_HSTS_PRELOAD = bool(os.getenv('DJANGO_SECURE_HSTS_PRELOAD', False))
# https://docs.djangoproject.com/en/3.1/ref/middleware/#x-content-type-options-nosniff
SECURE_CONTENT_TYPE_NOSNIFF = bool(os.getenv('DJANGO_SECURE_CONTENT_TYPE_NOSNIFF', False))

if IS_PROD:
    # https://docs.djangoproject.com/en/3.1/ref/settings/#secure-proxy-ssl-header
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
    # https://docs.djangoproject.com/en/3.1/topics/security/#ssl-https
    # https://docs.djangoproject.com/en/3.1/ref/settings/#secure-hsts-seconds
    # TODO: set this to 60 seconds first and then to 518400 once you prove the former works
    SECURE_HSTS_SECONDS = 60

# Email
# https://docs.djangoproject.com/en/3.1/ref/settings/#email-backend
EMAIL_BACKEND = os.getenv('DJANGO_EMAIL_BACKEND', 'django.core.mail.backends.console.EmailBackend')
# https://docs.djangoproject.com/en/3.1/ref/settings/#email-timeout
EMAIL_TIMEOUT = 5
# https://docs.djangoproject.com/en/3.1/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = os.getenv('DJANGO_DEFAULT_FROM_EMAIL', 'example@example.com')
# https://docs.djangoproject.com/en/3.1/ref/settings/#server-email
SERVER_EMAIL = os.getenv('DJANGO_SERVER_EMAIL', DEFAULT_FROM_EMAIL)
# https://docs.djangoproject.com/en/3.1/ref/settings/#email-subject-prefix
EMAIL_SUBJECT_PREFIX = os.getenv('DJANGO_EMAIL_SUBJECT_PREFIX', 'example ')

# Templates
# https://docs.djangoproject.com/en/3.1/ref/settings/#templates
TEMPLATES = [
    {
        # https://docs.djangoproject.com/en/3.1/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # https://docs.djangoproject.com/en/3.1/ref/settings/#template-dirs
        'DIRS': [str(ROOT_DIR / 'templates')],
        'OPTIONS': {
            # https://docs.djangoproject.com/en/3.1/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/3.1/ref/templates/api/#loader-types
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # https://docs.djangoproject.com/en/3.1/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    }
]

# Static
# https://docs.djangoproject.com/en/3.1/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR / 'staticfiles')
# https://docs.djangoproject.com/en/3.1/ref/settings/#static-url
STATIC_URL = '/static/'
# https://docs.djangoproject.com/en/3.1/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = []
# https://docs.djangoproject.com/en/3.1/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

# Media
# https://docs.djangoproject.com/en/3.1/ref/settings/#media-root
MEDIA_ROOT = str(ROOT_DIR / 'media')
# https://docs.djangoproject.com/en/3.1/ref/settings/#media-url
MEDIA_URL = '/media/'

# Fixtures
# https://docs.djangoproject.com/en/3.1/ref/settings/#fixture-dirs
FIXTURE_DIRS = (str(ROOT_DIR / 'fixtures'),)


# Your staff...
