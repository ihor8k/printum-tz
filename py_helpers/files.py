import os
import uuid
import random

from django.conf import settings
from django.utils.deconstruct import deconstructible


@deconstructible
class UploadAndRenameFile:
    def __init__(self, dir_name: str = None):
        self.dir_name = dir_name

    def __call__(self, instance, filename) -> str:
        path = '/'.join(list(str(random.randint(100, 1000)).zfill(2))[-2:])

        if self.dir_name:
            dir_path = str(settings.ROOT_DIR / self.dir_name)
            full_path = f'{dir_path}/{path}'
            path = f'{self.dir_name}/{path}'
        else:
            full_path = f'{settings.MEDIA_ROOT}/{path}'

        if not os.path.exists(full_path):
            os.makedirs(full_path)

        f, ext = os.path.splitext(filename)
        return '{}/{}{}'.format(path, uuid.uuid4().hex, ext)
