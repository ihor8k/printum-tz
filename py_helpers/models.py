import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _
from py_helpers.managers import ActiveManager, DeleteManager


class CreatedModel(models.Model):
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    objects = models.Manager()

    class Meta:
        abstract = True


class UpdatedModel(models.Model):
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)
    objects = models.Manager()

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if update_fields and 'updated_at' not in update_fields:
            update_fields.append('updated_at')
        return super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)


class DeletedModel(models.Model):
    is_delete = models.BooleanField(_('is deleted'), blank=True, default=False)
    objects = models.Manager()
    deleted = DeleteManager()

    class Meta:
        abstract = True

    def delete(self, using=None, keep_parents=False, force: bool = False):
        self.is_delete = True
        update_fields = ['is_delete']
        if hasattr(self, 'is_active'):
            self.is_active = False
            update_fields.append('is_active')
        self.save(update_fields=update_fields)


class ActiveModel(models.Model):
    is_active = models.BooleanField(_('is active'), blank=True, default=True)
    objects = models.Manager()
    active = ActiveManager()

    class Meta:
        abstract = True


class CreateUpdateActiveModel(CreatedModel, UpdatedModel, ActiveModel):
    objects = models.Manager()
    active = ActiveManager()

    class Meta:
        abstract = True


class UUIDModel(models.Model):
    id = models.UUIDField(_('id'), default=uuid.uuid4, primary_key=True, editable=False)
    objects = models.Manager()

    class Meta:
        abstract = True


class UUIDCreateUpdateActiveModel(UUIDModel, CreateUpdateActiveModel):
    objects = models.Manager()
    active = ActiveManager()

    class Meta:
        abstract = True
