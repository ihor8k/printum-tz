import re

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


class PhoneValidator:
    message = _('allowed phone number format: 380670000000, 380500000000!')
    code = 'invalid'
    c_regex = re.compile(r'^[0-9]{10,18}$')

    def __call__(self, value):
        if not self.c_regex.match(value):
            raise ValidationError(self.message, code=self.code)


def validate_phone(value: str) -> ValidationError:
    return PhoneValidator()(value)
