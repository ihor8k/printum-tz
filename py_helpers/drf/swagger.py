from collections import OrderedDict
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from drf_yasg import openapi
from drf_yasg.inspectors import SwaggerAutoSchema
from rest_framework import status


def base_response_empty_scheme(description: str = '') -> openapi.Response:
    description = description or _('An empty object returns.')
    return openapi.Response(description, openapi.Schema(type=openapi.TYPE_OBJECT, description=_('An empty object')))


def errors_response_scheme(description: str) -> openapi.Response:
    return openapi.Response(description, openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties=OrderedDict((
            ('errors', openapi.Schema(
                type=openapi.TYPE_ARRAY,
                items=openapi.Schema(type=openapi.TYPE_OBJECT, properties=OrderedDict(
                    (('detail', openapi.Schema(type=openapi.TYPE_STRING, description=_('Detail message.'))),
                     ('source', openapi.Schema(type=openapi.TYPE_STRING, description=_(f'Source field. {settings.SERIALIZER_FIELD_NULLABLE_HELP_TEXT}'))),
                     ('status', openapi.Schema(type=openapi.TYPE_INTEGER, description=_('HTTP Status Code'))),)
                ), required=['detail', 'source', 'status']),
                description=_('Error Information')
            )),
        )),
        required=['errors']
    ))


class BaseResponseAutoSchema(SwaggerAutoSchema):
    def get_responses(self) -> openapi.Responses:
        responses = super().get_responses()

        if not responses.keys():
            return responses

        status_code = list(responses.keys())[0]
        if status.HTTP_200_OK <= int(status_code) < status.HTTP_300_MULTIPLE_CHOICES:
            if status.HTTP_204_NO_CONTENT == int(status_code):
                return responses
            else:
                responses[status_code]['schema'] = {'allOf': [
                    openapi.Schema(type=openapi.TYPE_OBJECT, properties={'data': responses[status_code]['schema']}, description=_('Data Information')),
                ]}
        else:
            responses[status_code]['schema'] = {'allOf': [
                openapi.Schema(type=openapi.TYPE_OBJECT, properties={'errors': responses[status_code]['schema']}, description=_('Error Information')),
            ]}

        return responses


class LimitOffsetPaginationResponseAutoSchema(SwaggerAutoSchema):
    def get_responses(self) -> openapi.Responses:
        responses = super().get_responses()

        if not responses.keys():
            return responses

        status_code = list(responses.keys())[0]
        if status.HTTP_200_OK <= int(status_code) < status.HTTP_300_MULTIPLE_CHOICES:
            if status.HTTP_204_NO_CONTENT == int(status_code):
                responses[status_code]['schema'] = {'allOf': []}
            else:
                responses[status_code]['schema'] = {'allOf': [
                    openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={'data': responses[status_code]['schema'],
                                    'links': openapi.Schema(
                                        title=_('Links'),
                                        type=openapi.TYPE_OBJECT,
                                        required=['first', 'last'],
                                        properties=OrderedDict((
                                            ('first', openapi.Schema(type=openapi.TYPE_STRING, title=_('Link to first object'), read_only=True, format=openapi.FORMAT_URI)),
                                            ('last', openapi.Schema(type=openapi.TYPE_STRING, title=_('Link to last object'), read_only=True, format=openapi.FORMAT_URI)),
                                            ('next', openapi.Schema(type=openapi.TYPE_STRING, title=_('Link to next object'), read_only=True, format=openapi.FORMAT_URI)),
                                            ('prev', openapi.Schema(type=openapi.TYPE_STRING, title=_('Link to prev object'), read_only=True, format=openapi.FORMAT_URI))
                                        ))),
                                    'meta': openapi.Schema(
                                        title=_('Meta of result with pagination count'),
                                        type=openapi.TYPE_OBJECT,
                                        required=['count'],
                                        properties=OrderedDict((('count', openapi.Schema(type=openapi.TYPE_INTEGER, title=_('Number of results on page.'),)),))
                                    )},
                        description=_('Data Information')),
                ]}
        else:
            responses[status_code]['schema'] = {'allOf': [
                openapi.Schema(type=openapi.TYPE_OBJECT, properties={'errors': responses[status_code]['schema']}, description=_('Error Information')),
            ]}

        return responses


class MetaCountResponseAutoSchema(SwaggerAutoSchema):
    def get_responses(self) -> openapi.Responses:
        responses = super().get_responses()

        if not responses.keys():
            return responses

        status_code = list(responses.keys())[0]
        if status.HTTP_200_OK <= int(status_code) < status.HTTP_300_MULTIPLE_CHOICES:
            if status.HTTP_204_NO_CONTENT == int(status_code):
                responses[status_code]['schema'] = {'allOf': []}
            else:
                responses[status_code]['schema'] = {'allOf': [
                    openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={'data': responses[status_code]['schema'],
                                    'meta': openapi.Schema(
                                        title=_('Meta of result with count'),
                                        type=openapi.TYPE_OBJECT,
                                        required=['count'],
                                        properties=OrderedDict((('count', openapi.Schema(type=openapi.TYPE_INTEGER, title=_('Number of results.'),)),))
                                    )},
                        description=_('Data Information')),
                ]}
        else:
            responses[status_code]['schema'] = {'allOf': [
                openapi.Schema(type=openapi.TYPE_OBJECT, properties={'errors': responses[status_code]['schema']}, description=_('Error Information')),
            ]}

        return responses
