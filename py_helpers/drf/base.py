from typing import Dict, List, Any, Union
from rest_framework.status import HTTP_401_UNAUTHORIZED
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import APIException, AuthenticationFailed, NotAuthenticated
from rest_framework.pagination import LimitOffsetPagination as BaseLimitOffsetPagination
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param
from rest_framework.views import exception_handler


class LimitOffsetPagination(BaseLimitOffsetPagination):
    default_limit = 10
    max_limit = 100

    def get_first_link(self):
        url = self.request.build_absolute_uri()
        offset = 0
        first_link = replace_query_param(url, self.offset_query_param, offset)
        first_link = replace_query_param(first_link, self.limit_query_param, self.limit)
        return first_link

    def get_last_link(self):
        url = self.request.build_absolute_uri()
        offset = self.count - self.limit if (self.count - self.limit) >= 0 else 0
        last_link = replace_query_param(url, self.offset_query_param, offset)
        last_link = replace_query_param(last_link, self.limit_query_param, self.limit)
        return last_link

    def get_paginated_response(self, data):
        return Response({
            'meta': {'count': self.count},
            'links': {
                'first': self.get_first_link(),
                'next': self.get_next_link(),
                'previous': self.get_previous_link(),
                'last': self.get_last_link(),
            },
            'data': data,
        })


def _generate_errors_from_list(data: List, **kwargs) -> List:
    errors = []
    source = kwargs.get('source')
    status_code = kwargs.get('status_code', 0)
    for value in data:
        if isinstance(value, str):
            new_error = {'detail': value, 'source': source, 'status': status_code}
            errors.append(new_error)
        elif isinstance(value, list):
            errors.extend(_generate_errors_from_list(value, **kwargs))
        elif isinstance(value, dict):
            errors.extend(_generate_errors_from_dict(value, **kwargs))
    return errors


def _generate_errors_from_dict(data: Dict, **kwargs) -> List:
    errors = []
    source = kwargs.get('source')
    status_code = kwargs.get('status_code', 0)
    for key, value in data.items():
        source_val = f'{source}.{key}' if source else key
        if isinstance(value, str):
            new_error = {'detail': value, 'source': source_val, 'status': status_code}
            errors.append(new_error)
        elif isinstance(value, list):
            kwargs['source'] = source_val
            errors.extend(_generate_errors_from_list(value, **kwargs))
        elif isinstance(value, dict):
            kwargs['source'] = source_val
            errors.extend(_generate_errors_from_dict(value, **kwargs))
    return errors


def custom_exception_handler(exc: Any, context: Any) -> Response:
    response = exception_handler(exc, context)

    if response is not None:
        if isinstance(exc, (AuthenticationFailed, NotAuthenticated)):
            response.status_code = HTTP_401_UNAUTHORIZED
        errors = []
        data = response.data
        if isinstance(data, dict):
            errors.extend(_generate_errors_from_dict(data, status_code=response.status_code))
        elif isinstance(data, list):
            errors.extend(_generate_errors_from_list(data, status_code=response.status_code))
        response.data = {'errors': errors}
    else:
        #if settings.IS_PROD:
        #    capture_exception(exc)
        if settings.IS_DEV:
            raise exc
        exc = APIException(exc)
        response = exception_handler(exc, context)
        response.data = {'errors': [{'detail': _('Internal Server Error'), 'source': None, 'status': response.status_code}]}

    return response


def base_response(data: Union[Dict, List, None] = None, meta: Union[Dict, None] = None, **kwargs) -> Response:
    response_data = {'data': data or {}}
    if meta:
        response_data['meta'] = meta
    return Response(response_data, **kwargs)
