from django.conf import settings
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError


class LimitOffsetPaginationQueryParamsSerializerMixin(serializers.Serializer):
    limit = serializers.IntegerField(required=False, help_text=f'Number of data to return per page. {settings.SERIALIZER_FIELD_OPTIONAL_HELP_TEXT}')
    offset = serializers.IntegerField(
        required=False, help_text=f'The initial index from which to return the data. {settings.SERIALIZER_FIELD_OPTIONAL_HELP_TEXT}')

    def validate_limit(self, value):
        if value and value < 0:
            raise ValidationError(_('The value cannot be less than 0!'))
        return value

    def validate_offset(self, value):
        if value and value < 0:
            raise ValidationError(_('The value cannot be less than 0!'))
        return value


class ChoicesAdminFilterMixin:
    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({self.parameter_name: lookup}, []),
                'display': title,
            }
