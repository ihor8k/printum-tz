from django.db import models


class ActiveManagerMixin:
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)


class ActiveManager(ActiveManagerMixin, models.Manager):
    use_for_related_fields = True


class DeleteManagerMixin:
    def get_queryset(self):
        return super().get_queryset().filter(is_delete=True)


class DeleteManager(DeleteManagerMixin, models.Manager):
    use_for_related_fields = True
