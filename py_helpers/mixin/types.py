
class ExtendDescriptionEnumMixin:
    def __new__(cls, *args, **kwds):
        obj = object.__new__(cls)
        obj._value_ = args[0]
        return obj

    def __init__(self, val: str, description: str = None):
        self._description = description

    def __str__(self):
        return self.value

    @property
    def description(self):
        return self._description

    @property
    def id(self):
        return self._value_
