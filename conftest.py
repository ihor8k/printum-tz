import pytest

from django.db.models import QuerySet
from django.utils.timezone import now
from apps.accounts.models import Client
from apps.doctors.models import Doctor, WorkSchedule, Appointment


# accounts
from apps.doctors.types import ShiftType, Weekdays


@pytest.fixture()
def clients() -> QuerySet:
    Client.objects.create(first_name='Hovard', last_name='Volovitc')
    Client.objects.create(first_name='Sheldon', last_name='Cuper')
    Client.objects.create(first_name='Leonard', last_name='Hofstedder')
    return Client.objects.all()


# doctors
@pytest.fixture()
def doctors() -> QuerySet:
    Doctor.objects.create(first_name='Hovard', last_name='House')
    Doctor.objects.create(first_name='Harri', last_name='Potter')
    return Doctor.objects.all()


@pytest.fixture()
def work_schedule(doctors: QuerySet) -> QuerySet:
    for doctor in doctors:
        for day, label in Weekdays.choices:
            WorkSchedule.objects.create(doctor=doctor, day=day, shift=ShiftType.FIRST, start='09:00:00', end='13:00:00')
            WorkSchedule.objects.create(doctor=doctor, day=day, shift=ShiftType.LAST, start='14:00:00', end='18:00:00')
    return WorkSchedule.objects.all()


@pytest.fixture()
def appointments(doctors: QuerySet, clients: QuerySet) -> QuerySet:
    for doctor in doctors:
        Appointment.objects.create(doctor=doctor, client=clients.order_by('?').first(), date=now().date(), hour='09:00:00')
    return Appointment.objects.all()
