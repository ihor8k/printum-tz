## TZ


![](https://img.shields.io/badge/designed%20for-Printum-ff1544.svg)



**Need Install** [docker](https://docs.docker.com/install/)


Start local:

    docker-compose up

    ----/ or /----
    docker-compose up -d postgresql
    docker-compose up app

Shell:

    # python
    docker exec -it <your_conteiner_app> /bin/bash

    # postgresql
    docker exec -it <your_containet_db> bash

Run tests:

    docker exec -it <your_conteiner_app> pytest
